import React from 'react';
import './Button.css';

const Reset = (props) => {
  return <button onClick={props.reset} className='reset' >Reset</button>
};

export default Reset;