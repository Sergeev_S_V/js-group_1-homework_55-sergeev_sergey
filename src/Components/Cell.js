import React from 'react';
import './Cell.css';




const Cell = (props) => {

  return <span onClick={props.increase}
               className={props.cellClasses}>{props.key}</span>
};

export default Cell;