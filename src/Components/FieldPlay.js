import React from 'react';
import './FieldPlay.css';
import Cell from "./Cell";

const FieldPlay = (props) => {
  return (
    <div className='fieldPlay'>
      {props.cells.map(cell => <Cell key={cell.id}
                                     cellClasses={'cell ' + (cell.hasItem ? 'hasItem ' : '') + (!cell.visible ? 'hide' : '')}
                                     increase={() => props.increase(cell.id)}/>)}
      {props.start}
    </div>
  );
};

export default FieldPlay;
