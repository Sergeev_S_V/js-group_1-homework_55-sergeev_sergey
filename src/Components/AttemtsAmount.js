import React from 'react';
import './AttemptsAmount.css';

const Attempts = (props) => {
  return <p className='attempts'>Tries: {props.attempts}</p>
};

export default Attempts;