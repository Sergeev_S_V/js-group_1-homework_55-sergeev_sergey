import React, { Component } from 'react';
import './App.css';
import FieldPlay from "./Components/FieldPlay";
import Attempts from "./Components/AttemtsAmount";
import Button from "./Components/Button";
import Cell from "./Components/Cell";

class App extends Component {

  state = {
    cells: [],
    amountAttempts: 0
  };

  cellId = 0;

  generateCells = () => {
    let cells = [];
      for (let i = 1; i <= 36; i++) {
        let cell = {cell: <Cell/>, hasItem: false, id: this.cellId, visible: true};
        cells.push(cell);
        this.cellId++;
      }
    let randomId = Math.floor(Math.random() * cells.length);
    let filterId = (cell) => {
      return cell.id === randomId;
    };
    let filteredCells = cells.filter(filterId);
    let filteredCell = filteredCells[0];
    if (filteredCell.hasItem === false) {
      filteredCell.hasItem = true;
    }
    cells.splice(randomId, 1);
    cells.push(filteredCell);
    cells.sort((a,b) => a.id - b.id);
    console.log(filteredCell);
    console.log(cells);

    this.setState({cells});
  };

  increaseAmountAttempts = (id) => {
    let amountAttempts = this.state.amountAttempts;
    amountAttempts++;
    this.setState({amountAttempts});
    let cells = [...this.state.cells];
    let index = cells.findIndex(cell => cell.id === id);
    let cell = {...this.state.cells[index]};
    cell.visible = false;
    cells[index] = cell;

    this.setState({cells});
  };

  reset = () => {
    let currentAmount = {...this.state};
    currentAmount.amountAttempts = 0;

    this.setState(currentAmount);
  };





  render() {

      return (
        <div className="App">
          <button onClick={this.generateCells}>Start Game</button>
          <FieldPlay cells={this.state.cells}
                     increase={this.increaseAmountAttempts}
          />
          <Attempts attempts={this.state.amountAttempts}/>
          <Button reset={this.reset}/>
        </div>
      );
  }
}

export default App;
